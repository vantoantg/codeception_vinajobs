<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    /**
     * @param string $user
     * @param string $password
     * @param string $dir
     */
    public function loginAsAdmin($user = '', $password = '', $dir = '')
    {
        if(!$user || !$password) {
            $account = \Codeception\Util\Fixtures::get('admin_account');
            $user = $account['member'];
            $password = $account['password'];
        }

        $I = $this;
        $this->goToAdminPage($dir);

        $I->submitForm('#w0', [
            'AdminLoginForm[username]' => $user,
            'AdminLoginForm[password]' => $password
        ]);

        $I->see('Administrator', 'header#header .headerbar .headerbar-right ul li span.profile-info small');
    }

    public function goToAdminPage($dir = '')
    {
        $I = $this;
        if ($dir == '') {
            $config = \Codeception\Util\Fixtures::get('config');
            $I->amOnPage('/'.$config['admin_route']);
        } else {
            $I->amOnPage('/'.$dir);
        }
    }
}
