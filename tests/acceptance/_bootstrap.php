<?php

use Codeception\Util\Fixtures;

$config = parse_ini_file('tests/acceptance/config.ini', true);

Fixtures::add('admin_account', array(
    'member' => $config['admin_user'],
    'password' => $config['admin_password'],
));

Fixtures::add('config', $config);