<?php


class AdminDashboardCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->loginAsAdmin();
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function indexWorks(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('0932 252 414', '.header-connect .container .header-half.header-call span:first-child');
    }
}
